/*	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data 
in cards.json. Then pass control back to the code that called it by using a callback function.*/
const fs = require("fs");
const path = require("path");

function cardsInList(filePath, listId, callback) {
  setTimeout(() => {
    fs.readFile(path.join(__dirname, filePath), (err, data) => {
      if (err) {
        console.log("Error", err);
      } else {
        const resultArray = JSON.parse(data)[listId];

        callback(null, resultArray);
      }
    });
  }, 2 * 1000);
}

module.exports = cardsInList;

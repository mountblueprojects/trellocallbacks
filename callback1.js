/* Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards 
in boards.json and then pass control back to the code that called it by using a callback function.*/
const fs = require("fs");
const path = require("path");

function boardInformation(filePath, boardId, callback) {
  setTimeout(() => {
    fs.readFile(path.join(__dirname, filePath), (err, data) => {
      if (err) {
        console.log("Error", err);
      } else {
        const resultArray = JSON.parse(data);

        callback(
          null,
          resultArray.filter((item) => {
            return item.id === boardId;
          })[0]
        );
      }
    });
  }, 2 * 1000);
}

module.exports = boardInformation;

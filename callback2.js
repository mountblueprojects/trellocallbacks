/*Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. 
Then pass control back to the code that called it by using a callback function.*/

const fs = require("fs");
const path = require("path");

function listInformation(filePath, boardId, callback) {
  setTimeout(() => {
    fs.readFile(path.join(__dirname, filePath), (err, data) => {
      if (err) {
        console.log("Error", err);
      } else {
        resultArray = JSON.parse(data)[boardId];

        callback(null, resultArray);
      }
    });
  }, 2 * 1000);
}

module.exports = listInformation;

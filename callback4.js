/*Problem 4: Write a function that will use the previously written functions to get the following information. 
You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/
const fs = require("fs");
const path = require("path");
const boardInformation = require(path.join(__dirname, "callback1"));
const listInformation = require(path.join(__dirname, "callback2"));
const cardsInList = require(path.join(__dirname, "callback3"));

function getInformation1(boardPath, listPath, cardPath, boardName, listName) {
  setTimeout(() => {
    fs.readFile(path.join(__dirname, boardPath), (err, data) => {
      if (err) {
        console.log("Error", err);
      } else {
        boards = JSON.parse(data).filter((item) => {
          return item.name === boardName;
        });

        //After filtering, boards has only one element which is an object
        const boardId = boards[0].id;

        boardInformation(boardPath, boardId, (err, data1) => {
          if (err) {
            console.log("Error", err);
          } else {
            listInformation(listPath, boardId, (err, data2) => {
              if (err) {
                console.log("Error", err);
              } else {
                //After filtering, lists has only one element and its id is stored in listId
                const listId = data2.filter((item) => {
                  return item.name === listName;
                })[0].id;

                cardsInList(cardPath, listId, (err, data3) => {
                  if (err) {
                    console.log("Error", err);
                  } else {
                    console.log(data1);
                    console.log(data2);
                    console.log(data3);
                  }
                });
              }
            });
          }
        });
      }
    });
  }, 2 * 1000);
}

module.exports = getInformation1;

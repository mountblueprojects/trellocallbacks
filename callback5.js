/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const fs = require("fs");
const path = require("path");
const boardInformation = require(path.join(__dirname, "callback1"));
const listInformation = require(path.join(__dirname, "callback2"));
const cardsInList = require(path.join(__dirname, "callback3"));

function getInformation2(
  boardPath,
  listPath,
  cardPath,
  boardName,
  listName1,
  listName2
) {
  setTimeout(() => {
    fs.readFile(path.join(__dirname, boardPath), (err, data) => {
      if (err) {
        console.log("Error", err);
      } else {
        boards = JSON.parse(data).filter((item) => {
          return item.name === boardName;
        });

        //After filtering, boards has only one element which is an object
        const boardId = boards[0].id;

        boardInformation(boardPath, boardId, (err, data1) => {
          if (err) {
            console.log("Error", err);
          } else {
            listInformation(listPath, boardId, (err, data2) => {
              if (err) {
                console.log("Error", err);
              } else {
                //ListIds has only two elements corresponding to the listnames
                const listIds = data2.filter((item) => {
                  return item.name === listName1 || item.name === listName2;
                });

                cardsInList(cardPath, listIds[0].id, (err, data3) => {
                  if (err) {
                    console.log("Error", err);
                  } else {
                    cardsInList(cardPath, listIds[1].id, (err, data4) => {
                      if (err) {
                        console.log("Error", err);
                      } else {
                        console.log(data1);
                        console.log(data2);
                        console.log(data3);
                        console.log(data4);
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  }, 2 * 1000);
}

module.exports = getInformation2;
